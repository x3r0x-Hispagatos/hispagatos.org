+++
description = "Tutorial/howto de como acceder al nodo matrix de hispagatos"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title = "Hispagatos federated Matrix chat node Howto"
date = 2017-06-26T15:23:44-07:00
images = [
  "https://www.nyfa.edu/student-resources/wp-content/uploads/2014/12/HacktivistsWithANobleCause.jpg"
] # overrides the site-wide open graph image
+++

1. Hello this is a simple ""howto" to connect to the hispagatos.org matrix federated system node.
first  you need to download any of the [Matrix clients](https://matrix.to/#/#hispagatos:hispagatos.org)
2. You will need to ##register an account on our server matrix.hispagatos.org so we going to use as example
the riot-desktop app.
3. Download in your **blackarch GNU/linux** or any **Arch base GNU/Linux distro**
  + 1 pacman -S riot-desktop
  + 2 if you are in a Debian family distribution like Ubuntu [then follow this](https://riot.im/desktop.html)
4. ok now open it and click register like in the image below. Because as of now you are in "guest" mode
![Register new user](/images/register1.png) 
5. Now we have to add **OUR SERVER DETAILS** https://matrix.hispagatos.org
  + click "custom server" and if using any other client is similar, basically you have to click "advance", "custom server" or "connect to another node/server"... you get the idea.
![Select custom server](/images/custom-server2.png)
  + do not change the  **identity server** that is part of the federation settings.
![Add our server](/images/addourserver3.png)
  + yap yap now look for the register click/button etc and make sure our info is all correct with your username,passwd,email... **do not use phone number** well if you want to.. but is **NOT** needed.
![Add info](/images/addinfo4.png)
  + click/register
6. After this the system is going to send you a electronic mail(e-mail). This email will include a token in form of a link, **click on it** it will take you to the web client and maybe give you a CORS error, but **ignore it** in your desktop client **riot-deskopt** you should had been logged in and greeted by a funny bot with tutorials and help.
  + In a couple days going to enable a captcha in thie procedure, I will update the howto at the time. 
7. Ok now find channels to join, since is **federated** you will find #hispagatos #dc415 in the @hispagatos.org tab in the search box, to find federated channels switch tab/option to **matrix.org**
![Find channels](/images/channels5.png)
8. #hispagatos is invite only so when you are in the server let me know and I will invite you. I'm @rek2 just open a chat with me.
9. Contact: rek2@hispagatos.org
10. Please help out, we need a translation to Spanish and a Video tutorial in Spanish and English
**this howto is a beta**
11. You can also connect by IRC but is not encrypted and not recomended, server freenode, channel #hispagatos #dc415
12. remember we are hackers, RTFM! this is just a small howto to get you going, but this has so many more options and channels to discover from people all around the world, support decentralization and encryption!! a(A)a
13. VERYFY USER KEYS!!!, ok so when you enter #hispagatos since is a **encrypted** channel, you will need to verify everyones keys in the channel. for this sent a msg and click "**verify**" on each person keys, or right click on peoples avatar/nickname and select devices and **verify**. I will add a pic soon
14. and YES the green theme is uggly, if you are using RIOT and not another client, you can change to a dark them on the settings options for your user.
