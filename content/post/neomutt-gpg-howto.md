+++
description = "NeoMutt, GNUPG and Pass"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Neomutt GNUpg and Pass Howto"
date= 2019-02-02T23:58:37-08:00
images = [
  "https://gitlab.com/uploads/-/system/project/avatar/4868880/m2.png"
] # overrides the site-wide open graph image
+++

### NEOMUTT, GNUPG and Pass

* We are going to install neomutt a modern and configurable console imap/pop3/smtp client.
* We going to use GNUPG for decryption and encryption
* We going to be using Pass to store neomutt password

---

- [NeoMutt guide](https://neomutt.org/guide/)
- [GNUpg Documentation](https://www.gnupg.org/)
- [Pass Documentation](https://www.passwordstore.org/)

---

OK so let's get our hands dirty.

## Installation
I am using [BlackArch GNU/Linux](https://blackarch.org/) with **yay** but you can use **pacman**

```
yay -S neomutt
yay -S gnupg
yay -S pass

```

```
mkdir ~/.config/neomutt
cd ~/.config/neomutt
```
```
sudo updatedb
locate neomuttrc
cp /etc/neomuttrc ~/.config/neomutt/
```

Before we start, make sure you have your gnupg ready, this means all your friends and contacts
public keys and your private key or [onekey](https://onlykey.io/) etc setup if you have one.
If you are using [wayland](https://wayland.freedesktop.org/) and [sway](https://swaywm.org/) etc
I recommend setting up [dmenu](https://wiki.archlinux.org/index.php/dmenu) to promp for your key password.

To see a list of dmenu plugins and options:

```
yay -Ss dmenu
```

Now let's check for that **key ID**

```
gpg -k
gpg -K
```

this will list your public and secret keys in that order.

```
locate colors.linux
cp /usr/share/doc/neomutt/samples/colors.linux ~/.config/neomutt/
```

```
locate gpg.rc
cp /usr/share/doc/neomutt/samples/gpg.rc ~/.config/neomutt/
```

```
locate mailcap
cp /etc/mailcap ~/.config/neomutt/
```
---
## Configuration


Edit ~/.config/neomutt/neomuttrc

* here is an example configuration:

~/.config/neomutt/neomuttrc

```
# We source our color theme
source ~/.config/neomutt/colors.linux
# we can source as many files as needed if you like to divide configuration pieces by file instead
# of having a long configuration file.
#source ~/.config/neomutt/sidebar


# Here we run the local password manager and get the password for imap and smtp into a mutt variable
set mailcap_path = ~/.config/neomutt/mailcap
set my_pass="`pass neomutt/rek2`"

# Here are the general settings
set realname="ReK2"
set from="rek2@hispagatos.org"
set mail_check=90
set timeout=15


# IMAP
set imap_user="rek2@hispagatos.org"
set imap_pass=$my_pass
set folder=imaps://rek2@hispagatos.org
set spoolfile="+INBOX"

set sort=reverse-date
set sort_aux=last-date-received

#sidebar
set sidebar_visible = no
set sidebar_format = "%B%* %n"
set mail_check_stats
set sidebar_new_mail_only = no
set sidebar_width = 15
set sidebar_short_path = no

# --------------------------------------------------------------------------
# Note: All color operations are of the form:
#       color OBJECT FOREGROUND BACKGROUND
# Color of the current, open, mailbox
# Note: This is a general NeoMutt option which colors all selected items.
#color indicator cyan white
# Color of the highlighted, but not open, mailbox.
color sidebar_highlight white color8
# Color of the divider separating the Sidebar from NeoMutt panels
color sidebar_divider color8 white
# Color to give mailboxes containing flagged mail
color sidebar_flagged red white
# Color to give mailboxes containing new mail
color sidebar_new green white
# Color to give mailboxes containing no new/flagged mail, etc.
color sidebar_ordinary color245 default


# SMTP
set smtp_url="smtps://rek2@hispagatos.org:465"
set smtp_pass=$my_pass

# Source GNUPG settings to encrypt/decrypt/sign email
source ~/.config/neomutt/gpg.rc

auto_view text/html
```

- You can customize the settings and colors, but the most importat pieces here
is to configure your imap and smtp options.
- **NOTE**: your **SMTP** and **IMAP/POP3 configuration** may be different.
Follow the link above to the documentation.
- We create a variable that is going to pipe in the password from a encrypted local password manager
called pass


---
## SETTING UP PASS

If you have not yet imported or create your key pair(private and public keys) do so now
if you are moving your keys make sure you import them

```
gpg --import pgp-priv
gpg --import pgp-public
```

Now list your public keys and you will **need to get the id of your personal key**
the one you want to use to sign and decrypt.

```
gpg -k
```

An example will be the email associated with the key like:
  <rek2@hispagatos.org>

So then with that information you run:

```
pass init rek2@hispagatos.org
```

- This above initializes your encrypted password database.

Now **we have to trust the key** to be able to use it
so do this with your key id.

```
gpg --edit-key rek2@hispagatos.org
```

Once inside type:

* trust
* 5
* y

And that's it now is trusted.

Next we going to type your credentials, name or whatever you want to name this,
**you will later need this** on the neomutt config to retrieve the password

This below will prompt you for the password to be stored 2 times.. Do so :)

```
pass insert neomutt/rek2
```

Good! Now check the db with

```
pass neomutt/rek2
```

You should see your new entry :)
RTFM for how to use pass is very useful for many other applications.


---
## CONFIGURE GPG.RC


Here I have an example of my configuration, Of course changed my info
and added a random string s pgp key. You must enter here your pgp key
you want to use by default, there are other commands you can get from the
documentation to add diff keys, etc.

~/.config/neomutt/gpg.rc

```
set crypt_use_gpgme=yes
set crypt_autosign=yes
set crypt_verify_sig=yes
set crypt_replysign=yes
set crypt_replyencrypt=yes
set crypt_replysignencrypted=yes
set crypt_autoencrypt = yes


set pgp_default_key="FF095ACE1717888D5F81618BCE0E532EB65503DD"
set pgp_check_gpg_decrypt_status_fd
set pgp_use_gpg_agent = yes
set pgp_self_encrypt = yes

```

---

OK so every session you will have to enter your gpg private key password to open
your password database "pass" because pass ask gnupg for the key to open the database.
With out your pgp key you won't be able to open **pass** such why I recommend using
a external crypto device like onlykey, yubikey etc,
but is not needed but you will have your keys locally on your computer...

Remember this is only a very superficial way to get you going with all this three
tools working together, if up to you to become a power user and RTFM the manual
pages to really enjoy the things you can do with them.


Enjoy and please, contact me for typos/errata and grammar fix.

- [ReK2 in Keybase](https://keybase.io/rek2)
- [HISPAGATOS](https://hispagatos.org)
