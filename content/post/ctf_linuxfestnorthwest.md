+++
date = "2016-04-18T22:25:24-07:00"
draft = false
title = "CTF capture the flag at LinuxFest NorthWest 2016"

+++

**Capture the Flag Competition** with Hispagatos <a href="https://binaryfreedom.info">BinaryFreedom.info</a> and <a href="https://stealthy-cybersecurity.com">Stealthy CyberSecurity LLC</a>
we will be having a <a href="https://www.linuxfestnorthwest.org/2016/bofs/ctf-capture-flag-hispagatosorg-binaryfreedom-and-stealthy-cybersecurity">small CTF(capture the flag competition)</a> at
this years <a href="">LinuxFest NorthWest</a><br>

for more info go <a href="https://www.linuxfestnorthwest.org/2016/bofs/ctf-capture-flag-hispagatosorg-binaryfreedom-and-stealthy-cybersecurity">here</a>
