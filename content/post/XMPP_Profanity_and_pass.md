+++
description = "XMPP Jabber Profanity secure password with Pass"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "XMPP_Profanity_and_pass"
date= 2019-02-04T18:35:30-08:00
images = [
  "https://salsa.debian.org/uploads/-/system/project/avatar/12843/profanity.png"
] # overrides the site-wide open graph image
+++

<center>![Profanity logo](https://salsa.debian.org/uploads/-/system/project/avatar/12843/profanity.png)</center>

---

I been using Profanity for all my secure XMPP conversations, is like  [weechat](https://weechat.org/)/[Irssi client](http://www.irssi.org/) for [Jabber/XMPP](https://xmpp.org/).
I think is a great hacker tool to add to your GNU/Linux-fu magic since you can use it on so many
non-intended ways but that is not the aim of this short how to.

- For Profanity documentation go [here](http://www.profanity.im/userguide.html)
- For Pass documentation go [here](https://www.passwordstore.org/)

OK so let's go.

<center>![Profanity in action](http://www.profanity.im/images/prof-1.png)</center>

-----

So what I am going to show you is how to secure your password so is not on plain text, and if you are using a tiling window manager like [Sway](https://swaywm.org/) with [Wayland](https://wayland.freedesktop.org/) etc where we have no password graphical key chains.

SETTING UP PASS
===============

If you have not yet imported or create your key pair(private and public keys) do so now
if you are moving your keys make sure you import them. If you already been using this ignore this
part.

```
gpg --import pgp-priv
gpg --import pgp-public
```
Now list your public keys and you will **need to get the id of your personal key** to create
the Pass encrypted database to store your passwords.


```
gpg -k
```

An example will be the email associated with the key like:
  <rek2@hispagatos.org>

So if you never have initialized a Pass db before, run to create it:

```
pass init rek2@hispagatos.org
```

- This above initializes your encrypted password database.

Now **we have to trust the key** to be able to use it
so do this with your key id, again ignore this if you have your keys already trusted.

```
gpg --edit-key rek2@hispagatos.org
```

Once inside type:

* trust
* 5
* y

And that's it now is trusted.

Next we going to type your credentials, name or whatever you want to name this,
**you will later need this** on the Profanity configuration to retrieve the password

This below will prompt you for the password to be stored 2 times.. Do so :)

```
pass insert profanity/rek2
```

Good! Now check the db with

```
pass profanity/rek2
```

You should see your new entry :)
RTFM for how to use pass is very useful for many other applications.

Configure profanity
==================

You should read the documentation to setup your username/password and xmpp/jabber server
you going to connect to.
like:

```
/account add rek2
/account set rek2 jid rek2@xmpp.hispagatos.org
/account set rek2 server xmpp.hispagatos.org
/account set rek2 port 5111
```

OK this will set you up... But you STILL need to setup the password... For this
we have a cool option:

```
set <account> eval_password <command> : Shell command evaluated to retrieve password for the account. Can be used to retrieve password from keyring.
```

So this will be like:

```
/account set rek2 eval_password "pass profanity/rek2"
```

Now every time you login it will grab the password from the encrypted database of Pass
if is the first time you start your GnuPG session it will ask you for your PGP key password
to open up the key chain, such will use this key chain to authenticate with Pass and give profanity
the requested information, in this case the password...

OK that's it.. No more passwords on configuration files PLEASE.

[ReK2](https://keybase.io/rek2)
[Video Show](https://video.hispagatos.org/)


- Happy Hacking
- Hack The System
- Keep on Dreaming
