---
categories:
- blackarch
- infosec
- hacking
- pentesting
comments: true
date: 2015-11-12T00:00:00Z
title: BlackArch full install on a usb 3.0
url: /2015/11/12/blackarch-full-install-on-a-usb-3-dot-0/
---

what you need:

<ul>
  <li>one usb 3.0 stick with at least 100G to be able to install all your tools and personal scripts</li>
  <li>another usb any type or DVD/CD, this is for the install</li>
  <li>laptop or desktop computer</li>
  <li>latest blackarch live cd iso file <a href="http://blackarch.org/downloads.html">HERE</a></li>
</ul>


<p>Why? I do pentesting and linux forensics, I need always a couple USB drives with clean and secure usb sticks with me.
why USB 3? well, the speed of course, you wont even notice you are working from a usb. you could tecnically do this on a usb 2.x
but be ready to have a slow system.</p>
<br>

<b>Step 1</b>
burn the downloaded blackarch GNU/Linux live ISO into the generic USB stick or CD/DVD, this will create a live cd/usb
for you to boot into.
<p>
{{< highlight ruby >}} 
sudo dd bs=512M if=file.iso of=/dev/sdX
{{< / highlight >}}
</p>
change X for the target drive, usb or cd/dvd letter, example /dev/sdd

<b>Step 2</b>
Go to your laptop or desktop and boot into the BlackArch GNU/Linux live cd.
<br>
<b>Step 3</b>
When booted log in with <b>username</b> <i>root</i> and <b>password</b> <i>blackarch</i>
right click on the desktop for a menu, find "terminals" and open one.
<br>
<b>Step 4</b>

Connect your USB 3.x stick into your desktop or laptop, make sure it detects it and write down the device name.
{{< highlight ruby >}}
dmsg
{{< / highlight >}}
<p>make sure you do write down the device name, if not you could delete the local HD instead of writing to the USB!!!</p>

for the lastest version of blackarch-install, you can configure the network, that is out of scope but most likely a simple dhcp command will do.
then:
{{< highlight ruby >}}
sudo pacman -S blackarch-install-scripts
{{< / highlight >}}


now lets start the instalation:
{{< highlight ruby >}}
sudo blackarch-install
{{< / highlight >}}

<p>from here on you can just follow the instructions on the blackarch website, <b> when it ask for what device to do the instalation to, enter
the USB 3.x stick device, for example /dev/sdb</b>

<br>
Just follow the instalation instructions <a href="http://blackarch.org/downloads.html#install-iso">HERE</a>they are very easy, just<b> make sure you install
to the right device!!</b> 

note: if you running this on virtualbox later on, you prob have to run "VBoxClient-all && xrandr --output VGA-0 --auto" so the resolution auto changes when incrementing the window size.
note: more to be added, contact me for suggestions. handle = [rek2,rek2wilds,rek2gnulinux];  channel("#blackarch") on @freenode

