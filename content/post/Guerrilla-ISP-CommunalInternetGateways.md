+++
description = "Guerrilla ISP/Communal Internet gateways"
draft = false
toc = true
categories = ["technology", "hacktivism", "Hacktivismo", "hackers"]
tags = ["tech", "hacking", "hackers"]
title= "Guerrilla ISP CommunalInternetGateways"
date= 2017-11-24T00:23:02-08:00
images = [
  "https://btctheory.files.wordpress.com/2017/06/hacktivism-anarchism.jpg"
] # overrides the site-wide open graph image
+++

##  Guerrilla ISP/Communal Internet gateways ##

![Anarchist Hackers](https://btctheory.files.wordpress.com/2017/06/hacktivism-anarchism.jpg)

##### Hello/Hola Compañeros, Activistas, activists, hacktivists, cypher and cyber punks, comrades, freedom fighters, anarquistas, anarchists, social justice hackers,  free software advocates and other dreamers of a decentralized, free, libre universe including CyberSpace #####

First of all, you all know by know my English sucks :) so bare with me... thanks you amig@s.

I was talking to some compas on i2p and on other of our channels of anti-social communications, antisocial because it seems we like
the places nobody else likes.. we must be masochist or very good activists, because we dislike centralized, controlled by 3rd parties ways of communication... so beat it.
 
If net neutrality goes away, and honestly even if not, because, who wants a corporation OR a government to control the internet right? 

This brings me to a small note before I continue, I do not support goverment but I do not support neither corporations or any form of vertical close minded and oppressive forms. Unfortunately reality is that until we can get the majority of society to see what we see with example and building tools and spaces for this to happen, is that we have to some time compromise ( lol I must  be getting old ). Do not be full, as anarcho hackers we know net neutrality is not the solution just a 0 day patch.

well let's just say for the sake of taking the lesser evil liberal rhetoric, because what we have now is better than what 10 greedy companies will do if net neutrality goes away.. Government will still own the internet since they buy/own/push/bully the same corporations ( see the [cybersecurity complex][1]  [doing all the dirty work of the governments][2] that by law they can't do ), so be ready to use internet access as another form of struggle, prepare your communities and collectives, and specially your social justice circles, online and non-online, to chip in to form communal internet access or even Guerrila ISP's legal or illegal, access to information should not be weight on how deep are our wallets.

As a friendly reminder let me re-post [the Hacker ethics](https://ccc.de/hackerethics) that catches the spirit in what the internet was build.


    * Access to computers - and anything which might teach you something about the way the world really works - should be unlimited and total. Always yield to the Hands-On Imperative!
    * All information should be free.
    * Mistrust authority - promote decentralization.
    * Hackers should be judged by their acting, not bogus criteria such as degrees, age, race, or position.
    * You can create art and beauty on a computer.
    * Computers can change your life for the better.
    * Don't litter other people's data.
    * Make public data available, protect private data.



You know where to find us... come help in our community online so we can organize on our RL communities.. a(A)a

Con mucho amor y en la lucha <3(A)<3

[ReK2](https://keybase.io/cfernandez)



[1]: https://www.alternet.org/story/155764/6_government_surveillance_programs_designed_to_watch_what_you_do_online
[2]: http://www.imdb.com/title/tt7394178/
[3]: https://ccc.de/en/hackerethics
