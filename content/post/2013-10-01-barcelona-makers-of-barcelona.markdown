---
categories:
- opinion
- places
- livestyle
comments: true
date: 2013-10-01T00:00:00Z
title: Barcelona - Makers of Barcelona
url: /2013/10/01/barcelona-makers-of-barcelona/
---

<p>Recently I started my adventure into my nativeland <b>Spain</b>
more precisely <b>Barcelona</b> the place I was born and <b>Alicante</b>
the city I grow up most of my life.</p>

<p>There is a huge love for everything here, I missed that.. recently
I read an article that <a href="http://w110.bcn.cat/portal/site/Turisme/menuitem.7e257cdd389697aa58cb0d3020348a0c/?vgnextoid=e73b3a0a6ded0410VgnVCM1000001947900aRCRD&vgnextfmt=formatDetall&vgnextchannel=0000000495525130VgnV6CONT00000000200RCRD&lang=en_GB">Barcelona is the third happiest city in the world</a></p>
<p>Not going to even start with the passion and the night life...</p>
<p>OK! to the point... I was looking for a co-space to work with other tech lovers and such
and came to find a place called <b>Barcelona Makers</b> - it is yes a Maker/Hacker space...
but also a co-working space.. with all kind of classes and other activities</p>
http://www.mob-barcelona.com/
<p>If you come to Barcelona and in need to MAKE/HAck on things, just come over</p>



