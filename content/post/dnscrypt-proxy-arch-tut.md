+++
description = "How to setup dnscrypt-proxy"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "encryption", "dnscrypt", "dnssec", "hispagatos"]
title = "How to setup dnscrypt-proxy"
date = 2020-04-30T15:58:15-05:00
+++

# HOW TO - SETUP DNSCRYPT-PROXY

Here is a quick tutorial about [dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy) and how to set it up :)

## Getting Started

Welcome to a step-by-step dnscrypt-proxy tutorial brought to you by your local gatos y gatas. This was tested with Arch Linux *meow* and updated last on April 30, 2020. The majority of this will work with other Linux distros but we use Arch here.

### What is it?

The [Arch wiki page](https://wiki.archlinux.org/index.php/Dnscrypt-proxy) states it is "a DNS proxy with support for the encrypted DNS protocols DNS over HTTPS and DNSCrypt, which can be used to prevent man-in-the-middle attacks and eavesdropping. dnscrypt-proxy is also compatible with DNSSEC."

### Why use it?

DNS is a service that everyone uses but it transmits everything in plain text. This enables DNS to be used securely with encryption that not even your ISP can see your DNS requests :) You can tunnel requests thru known DNSSEC servers too.

Not ONLY to you get DNS encryption, but if configured correctly with [Anonymized-DNS](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Anonymized-DNS) can prevent intermediaries from recording and tampering with DNS traffic as it "prevents servers from learning anything about client IP addresses, by using intermediate relays dedicated to forwarding encrypted DNS data." This will be discussed in this tutorial.

You can also setup [filtering](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Filters) via blacklist, IP blacklist, and whitelist, which will be briefly discussed in this tutorial.

### Installing & Setup

This setup will use dnscrypt-proxy without IPv6 and without DNS-over-https, but feel free to change to your needs and I encourage you to experiment.

1) First install it:

```
	$ sudo pacman -S dnscrypt-proxy
```

2) Next we configure it as follows:

```
	$ sudo cp /etc/dnscrypt-proxy/dnscrypt-proxy.toml /etc/dnscrypt-proxy/dnscrypt-proxy.toml.ORIGIN
	$ sudo vim /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

* specify [server_names](https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v2/public-resolvers.md) that are privacy oriented:

```
	server_names = ['dnscrypt.eu-dk', 'dnscrypt.eu-nl', 'dnscrypt.uk-ipv4', 'ffmuc.net', 'meganerd', 'publicarray-au', 'scaleway-ams', 'scaleway-fr', 'v.dnscrypt.uk-ipv4']
```

* determine criteria of servers used:

```
	ipv4_servers = true
	ipv6_servers = false		# I don't use ipv6
	dnscrypt_servers = true
	doh_servers = false		# I disable DoH as not using any servers with it

	require_dnssec = true
	require_nolog = true
	require_nofilter = true
```

* set the response for blocked queries:

```
	blocked_query_response = 'refused'
```

* set the log to syslog:

```
	use_syslog = true
```

* (optional) create new & unique key for every single DNS query: [RTFM]

```
	dnscrypt_ephemeral_keys = true
```

* choose a [privacy-oriented fallback and netprobe address](https://gist.github.com/roge-/e7d926a3b3551a0f3769) - not that default G00GLE crap. Here we use Uncensored DNS, which is on [this "privacy DNS" list](https://waluszko.net/2018/04/10/pricacy-oriented-dns-providers-review/), but you can change to anything you'd like:

```
	fallback_resolvers = ['91.239.100.100:53']
	netprobe_address = '91.239.100.100:53'
```

* block the ipv6 here too:

```
	block_ipv6 = true
```

* expand on the "broken implementations" list to avoid certain servers:

```
	fragments_blocked = ['cisco', 'cisco-ipv6', 'cisco-familyshield', 'cisco-familyshield-ipv6', 'quad9-dnscrypt-ip4-filter-alt', 'quad9-dnscrypt-ip4-filter-pri', 'quad9-dnscrypt-ip4-nofilter-alt', 'quad9-dnscrypt-ip4-nofilter-pri', 'quad9-dnscrypt-ip6-filter-alt', 'quad9-dnscrypt-ip6-filter-pri', 'quad9-dnscrypt-ip6-nofilter-alt', 'quad9-dnscrypt-ip6-nofilter-pri', 'cleanbrowsing-adult', 'cleanbrowsing-family-ipv6', 'cleanbrowsing-family', 'cleanbrowsing-security']
```

* then setup our Anonymous DNS routes - [more info here](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Anonymized-DNS) and the [Anonymized DNS relays list here](https://github.com/DNSCrypt/dnscrypt-resolvers/blob/master/v2/relays.md):

```
	routes = [
    { server_name='dnscrypt.eu-dk', via=['anon-meganerd', 'anon-scaleway-ams'] },
    { server_name='dnscrypt.eu-nl', via=['anon-meganerd', 'anon-scaleway-ams'] },
    { server_name='dnscrypt.uk-ipv4', via=['anon-scaleway', 'anon-tiarap'] },
    { server_name='ffmuc.net', via=['anon-ibksturm', 'anon-scaleway-ams'] },
    { server_name='meganerd', via=['anon-scaleway', 'anon-tiarap'] },
    { server_name='publicarray-au', via=['anon-ibksturm', 'anon-tiarap'] },
    { server_name='scaleway-ams', via=['anon-scaleway', 'anon-meganerd'] },
    { server_name='scaleway-fr', via=['anon-meganerd', 'anon-v.dnscrypt.uk-ipv4'] },
    { server_name='v.dnscrypt.uk-ipv4', via=['anon-scaleway', 'anon-meganerd'] }
    ]

```

* then save config and exit

3) Next setup a blacklist (optional):

```
	$ sudo vim /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

* under the [blacklist] section, uncomment & set:

```
	blacklist_file = 'blacklist.txt'
```

* Create your own blacklist file from the [example](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-blacklist.txt) and [use this reference](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Public-blacklists).
There are other blacklists out there too. If you know me, then reach out and ask for the extreme one I use :)

* Also, you can uncomment 'log_file' in this section if you want a log of what is blacklisted. Very helpful for troubleshooting your blacklist.txt.

4) Next setup a whitelist (optional):

```
	$ sudo vim /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```

* under the [whitelist] section, uncomment & set:

```
	whitelist_file = 'whitelist.txt'
```

* Create your own whitelist file from the [example](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-whitelist.txt).

* Also, you can uncomment 'log_file' in this section if you want a log of what is whitelisted.

5) I encourage you to play with the IP blacklist, [example here](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-ip-blacklist.txt), but I have not used it yet.

6) Change /etc/resolv.conf to use dnscrypt-proxy:

```
	$ sudo vim /etc/resolv.conf
	nameserver 127.0.0.1		# make sure this is your nameserver
```

### Start and test
    
Run dnscrypt-proxy:

```
	$ sudo systemctl start dnscrypt-proxy.service
```

Check to see if it is now running on port 53

```
    $ ss -lp 'sport = :domain
```

Now test dnscrypt-proxy:

```
	$ sudo pkill -STOP dnscrypt-proxy
```
	
Then attempt to access a website and if not able to then your DNS traffic is successfully going through the proxy, so we turn it back on:

```
	$ sudo pkill -CONT dnscrypt-proxy
```

Optional test, go to [dnsleaktest.com](dnsleaktest.com), click on extended test, and then verify that it is working as the results will not display your ISP DNS but only ones in our configuration file.

### Option to start dnscrypt-proxy at boot:

If you want, set it to load @ boot:

```
	$ sudo systemctl enable dnscrypt-proxy.service
```

***
### Troubleshooting:

Q1: What if /etc/resolv.conf get's overwritten on reboot?

A1: Follow instructions above to change but also use the following command afterwards to make sure that resolv.conf does not get changed:

```
chattr +i /etc/resolv.conf
```

***
#### Some info to read:

(1) [dnscrypt-proxy source code](https://github.com/DNSCrypt/dnscrypt-proxy)

(2) [Arch Linux Wiki on dnscrypt-proxy](https://wiki.archlinux.org/index.php/Dnscrypt-proxy)

(3) [Installing dnscrypt-proxy on Linux](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Installation-linux) - from the creators

***
### Anything else...

Change whatever you need to & enjoy :) If there is anything else I missed then let me know. As always -> RTFM, hack the system & enjoy life! - Killab33z
