---
title: "Hackerñol en Kolektiva.Media"
author: "ReK2"
toc: true
date: 2020-07-20T18:49:54+02:00
description: "Hackerñol en Kolektiva.Media"
images: ["/images/Kolektiva.png"]
draft: false
---


- [Español](#español)
- [English](#english)

# Español

![Kolektiva Español](/images/Kolektiva.png)

Estamos contentos de poder anunciar que [Sub.media](https://sub.media/) junto con [Antimidia](https://antimidia.noblogs.org) nos invitaron para hospedar
los videos pasados, presentes y futuros de hispagatos, con los cual [Hackerñol](https://kolektiva.media/video-channels/hackernol/videos).

* Para subscribiros a nuestro canal desde mastodon y noticias añadir:
  - hackernol@kolektiva.media
  - hispagatos@kolektiva.media
  - colectivo@hispagatos.space
  - rek2@hispagatos.space
  - kolektiva@mastodon.online

* Por lo cual tendremos dos presencias:
  - [Hackerñol en Lbry](https://open.lbry.com/@Hackernol:7?r=ATqhndQnpGqW3Sy6LhGRDxqgEMbe8Ugv) que es donde podemos financiarnos un poco gracias a vosotros
  - [Hackerñol en Kolektiva](https://kolektiva.media/video-channels/hackernol)

---

Kolektiva
=========
Kolektiva es una plataforma de código abierto para alojar videos anarquistas de todo el mundo. El objetivo de Kolektiva es ayudar a aumentar la comunicación y la solidaridad a través de las fronteras y las barreras lingüísticas. Si estás interesade en participar, ya sea alojando tu contenido o ayudando con la traducción, por favor contacta con kolektivamedia@riseup.net
---
El video de presentacion de [Kolektiva](https://kolektiva.media/) en Español:
{{< kolektiva fa99b191-40ff-441e-ba88-acb5cc628e79 >}}


# English

![Kolektiva English](/images/Kolektiva-en.png)

We are happy to announce that [Sub.media](https://sub.media/) together with [Antimidia](https://antimidia.noblogs.org) invited us to host
the past, present and future videos of Hispagatos, with which [Hackerñol](https://kolektiva.media/video-channels/hackernol/videos) is part of.

* To subscribe to our channel from mastodon add:
   - hackernol@kolektiva.media
   - hispagatos@kolektiva.media
   - colectivo@hispagatos.space
   - rek2@hispagatos.space
   - kolektiva@mastodon.online

* For which we will have two presences:
   - [Hackerñol en Lbry](https://open.lbry.com/@Hackernol:7?r=ATqhndQnpGqW3Sy6LhGRDxqgEMbe8Ugv) which is where we can finance ourselves a bit thanks to you
   - [Hackerñol in Kolektiva](https://kolektiva.media/video-channels/hackernol)

---
Kolektiva
=========
Kolektiva is an open-source platform for hosting anarchist videos from around the world. Our goal with Kolektiva is to help increase communication and material solidarity across borders and linguistic divides. If you are interested in getting involved - whether through hosting your content with us, or helping out with translation - please contact us at kolektivamedia@riseup.net.
---

The presentation video of [Kolektiva](https://kolektiva.media/) in English:
{{< kolektiva bcb4bbfa-c39d-4fd2-96cf-bc4747d2d9d9 >}}


# Links
## Hispagatos
---

- [Hispagatos blog](https://hispagatos.org)
- [Hispagatos hub](https://hubs.mozilla.com/zWXK8U6/hispagatos/)
- [Hispagatos lemmy](https://dev.lemmy.ml/c/hispagatos)
- [Hackerñol lbry](https://open.lbry.com/@Hackernol:7)
- [Hackerñol bittube](https://bittube.video/video-channels/hackernol/)
- [Hackstory](https://www.hackstory.net/BBK)
- [Rek2 mastodon](https://hispagatos.space/@rek2)
- [Rek2 keybase](https://keybase.io/rek2)
- [Rek2 bittube](https://bittube.video/accounts/rek2)
- [t1b0 mastodon](https://hispagatos.space/@_1b0)
- [t1b0 keybase](https://keybase.io/t1b0)
- [sigma0100 mastodon](https://hispagatos.space/@sigma4)
- [sigma0100 keybase](https://keybase.io/sigma0100)
- [MegageM mastodon](https://hispagatos.space/@MegxgeM)
- [MegageM keybase](https://keybase.io/mggm)
- [MegageM bittube](https://bittube.video/accounts/)
- [real-changeling mastodon](https://deadinsi.de/@forAll52)
- [real-changeling keybase](https://keybase.io/realchangeling)
- [Killab33z keybase](https://keybase.io/killab33z)
- [Killab33z mastodon](https://hispagatos.space/@Killab33z_OG)
- [Trizz](https://keybase.io/tcregger)

## The movement
---

- [Anarcho Hackers Wiki ESP](https://www.noisebridge.net/wiki/Anarcohackers)
- [Anarcho Hackers Wiki](https://www.noisebridge.net/wiki/Anarchisthackers/)
- [Manifiesto Anarco Hacker ESP](https://hispagatos.org/post/traduccionmanifiestoanarcohacker/)
- [Anarcho Hackers Manifesto 2018](https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018)
- [Anarcho\_hackers lemmy](https://dev.lemmy.ml/c/anarcho_hackers)
- [Hackbloc lemmy](https://dev.lemmy.ml/c/hackbloc)
- [La Ética Hacker ESP](https://hispagatos.org/post/la_etica_hacker/)
- [Guerilla Open Access Manifesto ESP](https://hispagatos.org/post/guerilla-open-access-manifesto/)
- [The Hacker Crackdown: Law and Disorder on the Electronic Frontier](https://en.wikipedia.org/wiki/The_Hacker_Crackdown)
- [Hackers: Heroes of the Computer Revolution](https://en.wikipedia.org/wiki/Hackers:_Heroes_of_the_Computer_Revolution)
- [Libertarian Socialism](https://en.wikipedia.org/wiki/Libertarian_socialism)
- [Anti-Authoriarianism](https://en.wikipedia.org/wiki/Anti-authoritarianism)
- [Anarcho-Communism](https://en.wikipedia.org/wiki/Anarcho-communism)

Sobre Hispagatos:
Trabajamos duro para preservar la descentralización, seguridad y privacidad en el ciberespacio y motivar una sociedad tecno anarco comunista (STAC), horizontal y no jerarquizada donde la t
ecnología es hecha por personas para personas y no por corporaciones para controlar personas. a(A)a

About Hispagatos:
We work hard to preserve decentralization,security and privacy in cyberspace and motivate towards an horizontal and non hierarchical techno-anarcho-communist society (TACS) where technology
 is made by people for the people not by corporate masters to control people. a(A)a

