+++
description = "La ética hacker"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "La ética hacker"
date= 2019-11-20T02:49:01+01:00
images = [
  "https://www.degenerationit.com/wp-content/uploads/2016/05/steven-levy-hackers-heroes-of-the-computer-revolution-1.jpg"
] # overrides the site-wide open graph image
+++

![hackers heroes of the computer revolution](/images/steven_levy.jpg)

Gracias al gran archivo de cultura hacker del proyecto TCP "The CyberPunk Proyect"
Podeis encontrar el texto original en ingles [aqui](http://project.cyberpunk.ru/idb/hacker_ethics.html).

La idea de una "ética hacker" quizás está mejor desarrollada en el libro de 1984 de Steven Levy, [Hackers: Heroes of the Computer Revolution](https://www.stevenlevy.com/index.php/books/hackers). Levy, despues de estudiar la cultura hacker, formuló seis principios éticos:

    1. El acceso a las redes, ordenadores, y a cualquier otra cosa que pueda enseñarte algo sobre cómo funciona el mundo, debe ser ilimitado y total. ¡Anteponga siempre el uso practico!
    2. Toda información debe ser libre.
    3. Desconfiar de la autoridad: promover la descentralización.
    4. Los hackers deben ser juzgados por su hacking, no por criterios falsos, como nivel educativo, raza, titulo, diploma, edad, clase, carrera o posición.
    5. Se puede crear arte y belleza en una computadora.
    6. Las computadoras pueden cambiar la vida para mejor.



[PHRACK](http://phrack.org/), reconocido como el boletín "oficial" de Phreaking y hacking, amplió este credo justificando que se puede resumir en tres principios ("Doctor Crash", 1986).
[1] Primero, los hackers rechazan la idea de que las "empresas" son los únicos grupos con derecho al acceso y uso de las nuevas tecnologías.
[2] Segundo, hackear es un arma importante en la lucha contra la usurpación de la tecnología por gobiernos y las grandes empresas.
[3] Finalmente, el alto costo de los equipos informáticos está más allá de los medios economicos de la mayoría de hackers, lo que da como resultado la percepción de que el hacking y el phreaking son el único recurso para difundir el conocimiento informático a las masas:

> "Hackear. Es un hobby a tiempo completo, teniendo que dedicar incontables horas a la semana para aprender, experimentar y ejecutar el arte de penetrar ordenadores multiusuario: ¿Por qué los hackers pasan una buena parte de su tiempo hackeando?
> Algunos podrían decir que es curiosidad científica, otros que es por la estimulación mental. Pero las verdaderas razones de los motivos de los hackers son mucho más profundas que eso. En este archivo describiré los motivos subyacentes de los hackers, daré a conocer las conexiones entre Hacking, Phreaking, Carding y Anarquia, y daré a conocer la "revolución tecnológica" que se está implantando detras de la mente de cada hacker...
> Si necesitas un tutorial sobre cómo realizar cualquiera de los métodos indicados anteriormente {de hacking}, porfavor lealo en el e-zine de [{PHRACK}](http://phrack.org/). Y hagas lo que hagas, continúa con la lucha. Lo creas o no, si eres un hacker, eres un revolucionario. No te preocupes, estás en el lado correcto". ("Doctor Crash", 1986).

Aunque los hackers reconocen libremente que sus actividades pueden ser a veces ilegales, ponen un considerable énfasis en romper las restricciones impuestas para obtener acceso y aprender un sistema, y muestran hostilidad hacia aquellos que transgreden más allá de estos límites como robar dinero con ánimo de lucro personal.
Los miembros veteranos más experimentados desconfian de los jóvenes novatos que a menudo están fascinados con lo que perciben como el "romance" de el hacking. La élite hacker se queja continuamente de que los novatos corren un mayor riesgo de ser descubiertos y pueden "echar a perder" las cuentas y el trabajo realizado en las que los hackers experimentados han obtenido y podido ocultar su acceso.
 
En resumen, el espiritu hacker refleja objetivos bien definidos, redes de comunicación, valores y un espíritu de resistencia a la autoridad. 
Debido a que el hacking requiere una gama más amplia de conocimiento que el phreaking, y debido a que dicho conocimiento solo se puede adquirir a través de la experiencia, los hackers tienden a ser más viejos y con mas conocimiento que los phreakers. 
Además, a pesar de cierta superposición, los objetivos de los dos grupos son diferentes. Como consecuencia, cada grupo constituye una categoría analítica separada.
 


Esto es de [Richard Stallman](https://stallman.org/), quien se formó en el Laboratorio de IA (Inteligencia Artificial) del M.I.T. en 1971, donde, a finales de los maravillosos años 60s, el hacking estalló. Quizás sea mejor conocido por haber escrito la madre de todos los programas libres, un editor de texto conocido como EMACS.

> "No sé si realmente existe una ética hacker como tal, pero seguro que existía una ética en el Laboratorio de Inteligencia Artificial del MIT. Esta era que no se debería permitir que la burocracia se interpusiera en el camino de hacer algo útil. 
> Las reglas no importaban: los resultados, sí. Reglas, en forma de seguridad informática o cerraduras en las puertas, se consideraban como una total falta de respeto. 
> Nos sentiamos orgullosos de la rapidez con la que eliminabamos cualquier pequeña burocracia que se interpusiera en el camino y el poco tiempo que te hacía desperdiciar. 
> Cualquiera que se atreviera a cerrar una terminal en su oficina, por decir que era un profesor y pensar que era más importante que otras personas, probablemente encontraría su puerta abierta a la mañana siguiente. 
> Simplemente yo subiría por el techo o por debajo del suelo, y moveria la terminal afuera, o dejaria la puerta abierta con una nota que diga que es un gran inconveniente tener que pasar debajo del suelo, "así que no moleste a las personas cerrando la puerta nunca mas". 
> Incluso ahora , hay una gran llave inglesa en el Laboratorio de IA, titulada "la llave maestra del séptimo piso", que se utilizará en caso de que alguien se atreva a bloquear uno de los terminales más interesantes".

Texto compartido por ["The Cyberpunk Project](http://project.cyberpunk.ru/).

Traducido por Perromalo, subido por MegageM hispagatos.org y arreglado por ReK2 y MegageM ;)
