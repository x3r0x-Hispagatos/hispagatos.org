== Hispagatos - Anarchist hacker collective ==

**Hispagatos news server**

- **Blog** https://hispagatos.org
- **Videos** https://kolektiva.media/video-channels/hackernol/videos
- **Videos2** https://open.lbry.com/@Hackernol:7?r=ATqhndQnpGqW3Sy6LhGRDxqgEMbe8Ugv
- **Mastodone** https://hispagatos.space

    **a(A)a**  
Hack and Decentralize The System

    **Hispagatos**

 Donate using Liberapay:

- https://liberapay.com/Hispagatos/donate

Donate using Monero:

- 4AYprKT26fnZYdY1JjKLMR2K7zmCQhe9i5bn5oMV4VtEYvjiFhhXLVq8iycHRLTr6cD3oVSq19Yrx4JfmuuaU453M8GzTKb

Donate using BTC:

- 1PLsUqNB8YjPHNSYcNfvtkqoFkPTFhLoAh


== Hispagatos - Anarchist Hacker Collective ==
